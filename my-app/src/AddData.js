import React from 'react';
import FileBase64 from 'react-file-base64';
import { Row, Form, Col, Button } from 'react-bootstrap';

class AddData extends React.Component {
  constructor(props) {
    super(props);
    this.initialState = {
      idUsrtrx: '',
      no: '',
      userid:'',
      trxid:'',
      image: '',
    }

    if(props.product){
      this.state = props.product
    } else {
      this.state = this.initialState;
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleChange(event) {
    const name = event.target.name;
    const value = event.target.value;

    this.setState({
      [name]: value
    })
  }

  handleSubmit(event) {
    let checkImg = this.state.files !== undefined ? this.state.files[0].base64.replace(/^data:image\/[a-z]+;base64,/, "")
            : this.state.image;
    this.state.image= checkImg;
    event.preventDefault();
    this.props.onFormSubmit(this.state);
    this.setState(this.initialState);
  }

  getFiles(files) {
    // console.log('', files[0].replace('data:image/jpeg;base64,'));

    this.setState({ ...this.state, files: files, flagimage: 1 })
    // console.log('aa', this.state.files[0]);
}


  render() {

    let pageTitle;
    if(this.state.idUsrtrx) {
      pageTitle = <h2>Edit Transaksi</h2>
    } else {
      // console.log("Add=======");
      pageTitle = <h2>Add Transaksi</h2>
    }

    return(
      <div>
        {pageTitle}
        <Row>
          <Col sm={6}>
            <Form onSubmit={this.handleSubmit}>
            <Form.Group controlId="no">
                <Form.Label>NO</Form.Label>
                <Form.Control
                  type="text"
                  name="no"
                  value={this.state.no}
                  onChange={this.handleChange}
                  placeholder="NO" />
              </Form.Group>
              <Form.Group controlId="userid">
                <Form.Label>USER ID</Form.Label>
                <Form.Control
                  type="text"
                  name="userid"
                  value={this.state.userid}
                  onChange={this.handleChange}
                  placeholder="user id" />
              </Form.Group>
              {/* <Form.Group controlId="trxid">
                <Form.Label>TRX ID</Form.Label>
                <Form.Control
                  type="text"
                  name="trxid"
                  value={this.state.trxid}
                  onChange={this.handleChange}
                  placeholder="Trx Id"/>
              </Form.Group> */}
              <Form.Group controlId="image">
                <Form.Label>Image</Form.Label>
                <FileBase64
                                    multiple={true}
                                    onDone={this.getFiles.bind(this)} />
                
                {/* <Form.Control
                  type="text"
                  name="image"
                  value={this.state.image}
                  onChange={this.handleChange}
                  placeholder="image" /> */}
              </Form.Group>              
              <Form.Group>
                <Form.Control type="hidden" name="idUsrtrx" value={this.state.idUsrtrx} />
                <Button variant="success" type="submit">Save</Button>
              </Form.Group>
            </Form>
          </Col>
        </Row>
      </div>
    )
  }
}

export default AddData;
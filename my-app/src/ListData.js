import React from 'react';
import { Table, Button, Alert } from 'react-bootstrap';
import { saveAs } from 'file-saver';

class ListData extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            error: null,
            products: []
        }
    }

    componentDidMount() {
        const apiUrl = 'http://localhost:8080/mti/view';

        fetch(apiUrl)
            .then(res => res.json())            
            .then(                
                (result) => {
                    // console.log('download', result);
                        this.setState({
                            products: result
                        });
                }
            )
    }

    deleteProduct(idUsrtrx) {
        const { products } = this.state;

        const apiUrl = 'http://localhost:8080/mti/delete?id=' + idUsrtrx;
        const options = {
            method: 'GET',
            headers: {
                "Content-Type": "application/json"
            },
        }

        fetch(apiUrl, options)
            .then(res => res)
            .then(
                (result) => {
                    this.setState({
                        response: result,
                        products: products.filter(product => product.idUsrtrx !== idUsrtrx)
                    });
                },
                (error) => {
                    this.setState({ error });
                }
            )
    }

    download_file(idUsrtrx) {
        const apiUrl = 'http://localhost:8080/mti/download?id=' +idUsrtrx;

        fetch(apiUrl)
            // .then(res => res)
            .then(
                (result) => {
                    // console.log('downlad', result.blob());
                    // var binaryData = [];
                    // binaryData.push(result.blob());
                    // window.URL.createObjectURL(new Blob(binaryData, {type: "application/octet-stream"}))
                    // console.log('downlad', binaryData);
                    saveAs(result.blob(), "image_transaksi.jpg")
                }
            )
    }

    render() {
        const { error, products } = this.state;

        if (error) {
            return (
                <div>Error: {error.message}</div>
            )
        } else {
            return (
                <div>
                    <h2>Transaksi User</h2>
                    <Table>
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>User ID</th>
                                <th>TRX ID</th>
                                <th>Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            {products.map(product => (
                                <tr key={product.idUsrtrx}>
                                    <td>{product.no}</td>
                                    <td>{product.userid}</td>
                                    <td>{product.trxid}</td>
                                    <img width='100px' height='100px' src={"data:image/jpeg;base64," + product.image} alt="image" />
                                    <td>
                                        <Button variant="info" onClick={() => this.props.editProduct(product.idUsrtrx)}>Edit</Button>
                                        <Button variant="danger" onClick={() => this.deleteProduct(product.idUsrtrx)}>Delete</Button>
                                        <Button variant="danger" onClick={() => this.download_file(product.idUsrtrx)}>Download</Button>
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </div>
            )
        }
    }
}

export default ListData;
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import { Container, Button, Alert } from 'react-bootstrap';
import ListData from './ListData';
import AddData from './AddData';

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAddProduct: false,
      error: null,
      response: {},
      product: {},
      isEditProduct: false
    }
    this.onFormSubmit = this.onFormSubmit.bind(this);
  }

  onCreate() {
    this.setState({ isAddProduct: true });
  }

  onFormSubmit(data) {
    let apiUrl;

    if (this.state.isEditProduct) {
      apiUrl = 'http://localhost:8080/mti/edit';
    } else {
      apiUrl = 'http://localhost:8080/mti/add';
    }

    const myHeaders = new Headers();
    myHeaders.append('Content-Type', 'application/json');

    const options = {
      method: 'POST',
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(data)

    };

    fetch(apiUrl, options)
      .then(res => res)
      .then(result => {
        this.setState({
          response: result,
          isAddProduct: false,
          isEditProduct: false
        })
      },
        (error) => {
          this.setState({ error });
        }
      )
  }

  editProduct = idUsrtrx => {

    const apiUrl = 'http://localhost:8080/mti/getsingle?id=' + idUsrtrx;
    const options = {
      method: 'GET'
    }

    fetch(apiUrl, options)
      .then(res => res.json())
      .then(
        (result) => {
          console.log("resultsingle==", result);
          this.setState({            
            product: result,
            isEditProduct: true,
            isAddProduct: true
          });
        },
        (error) => {
          this.setState({ error });
        }
      )
  }


  render() {
    let productForm;
    if (this.state.isAddProduct || this.state.isEditProduct) {
      productForm = <AddData onFormSubmit={this.onFormSubmit} product={this.state.product} />
    }

    return (
      <div className="App">
        <Container>
          {this.state.response.status === 'success' && <div><br /><Alert variant="info">{this.state.response.message}</Alert></div>}
          {!this.state.isAddProduct && <ListData editProduct={this.editProduct} />}
          {!this.state.isAddProduct && <Button variant="primary" onClick={() => this.onCreate()}>Add Transaksi</Button>}
          {productForm}
          {this.state.error && <div>Error: {this.state.error.message}</div>}
        </Container>
      </div>
    );
  }
}

export default App;

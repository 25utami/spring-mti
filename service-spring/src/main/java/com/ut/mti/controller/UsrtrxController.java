package com.ut.mti.controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletContext;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.CrossOrigin;

import com.ut.mti.constant.VariabelConstant;
import com.ut.mti.entity.Usrtrx;
import com.ut.mti.service.UsrtrxDAO;

@RestController
public class UsrtrxController {
	@Autowired
	UsrtrxDAO service;
	
	private ServletContext servletContext;
	 
	@GetMapping(VariabelConstant.VIEW)
	@CrossOrigin(origins = "*")
	public List<Usrtrx> view(){
		return service.getAll();
	}
	
	@PostMapping(VariabelConstant.ADD)
	@CrossOrigin(origins = "*")
	public String add(@RequestBody Usrtrx usrtrx){
		String trxNum = service.trxNumber();
		usrtrx.setTrxid(trxNum);
		service.save(usrtrx);
		return "Sukses Menambahkan Transaksi";
	}
	
	@PostMapping(VariabelConstant.EDIT)
	@CrossOrigin(origins = "*")
	public String edit(@RequestBody Usrtrx usrtrx){
		Usrtrx cekTrx = service.getById(usrtrx.getIdUsrtrx());
		if(cekTrx == null) return "Transaksi Tidak Ditemukan";
		usrtrx.setIdUsrtrx(cekTrx.getIdUsrtrx());
		service.save(usrtrx);
		return "Sukses Mengubah Transaksi";
	}
	
	@GetMapping(VariabelConstant.DELETE)
	@CrossOrigin(origins = "*")
	public String delete(@RequestParam Integer id){
		Usrtrx cekTrx = service.getById(id);
		if(cekTrx == null) return "Transaksi Tidak Ditemukan";
		service.delete(id);
		return "Transaksi Berhasil Dihapus";
	}
	
	@GetMapping(VariabelConstant.GET_SINGLE)
	@CrossOrigin(origins = "*")
	public ResponseEntity<Usrtrx> getSIngle(@RequestParam Integer id){
		Usrtrx cekTrx = service.getById(id);
		if(cekTrx == null) return ResponseEntity.notFound().build();
		return ResponseEntity.ok(cekTrx);
	}
	
	@GetMapping(VariabelConstant.DOWNLOAD)
	@CrossOrigin(origins = "*")
	public ResponseEntity downloadFromDB(@RequestParam Integer id) {
		Usrtrx cekTrx = service.getById(id);
		byte[] imageByte=Base64.decodeBase64(cekTrx.getImage());
		String fileName = cekTrx.getTrxid() + cekTrx.getUserid();
		return ResponseEntity.ok()
				.contentType(MediaType.parseMediaType("application/octet-stream"))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + fileName + "\"")
				.body(imageByte);
	}
	
	/*@GetMapping(VariabelConstant.DOWNLOAD)
	@CrossOrigin(origins = "*")
	public String downloadFromDB(@RequestParam Integer id) throws FileNotFoundException, IOException {
		Usrtrx cekTrx = service.getById(id);
		byte[] imageByte=Base64.decodeBase64(cekTrx.getImage());
		String directory=servletContext.getRealPath("/")+"images/sample.jpg";

        new FileOutputStream(directory).write(imageByte);
        return "success ";
	}*/

}

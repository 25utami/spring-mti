package com.ut.mti.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity(name ="Usrtrx")
@Table(name = "usrtrx")
public class Usrtrx {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer idUsrtrx;
	
	@Column(name = "NO")
	private Integer no;
	
	@Column(name = "USERID")
	private String userid;
	
	@Column(name = "TRXID")
	private String trxid;
	
	@Column(name = "IMAGE")
	private String image;

	public Integer getIdUsrtrx() {
		return idUsrtrx;
	}

	public void setIdUsrtrx(Integer idUsrtrx) {
		this.idUsrtrx = idUsrtrx;
	}

	public Integer getNo() {
		return no;
	}

	public void setNo(Integer no) {
		this.no = no;
	}

	public String getUserid() {
		return userid;
	}

	public void setUserid(String userid) {
		this.userid = userid;
	}

	public String getTrxid() {
		return trxid;
	}

	public void setTrxid(String trxid) {
		this.trxid = trxid;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public Usrtrx() {
		
	}
	
	public Usrtrx(Integer no, String userid, String trxid, String image) {
		this.image=image;
		this.no=no;
		this.trxid=trxid;
		this.userid=userid;
	}

}

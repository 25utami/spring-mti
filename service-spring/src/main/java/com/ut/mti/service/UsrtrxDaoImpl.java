package com.ut.mti.service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ut.mti.entity.Usrtrx;
import com.ut.mti.repository.UsrtrxRepository;

@Service
public class UsrtrxDaoImpl implements UsrtrxDAO{
	@Autowired
	UsrtrxRepository usrtrxRepository;

	@Override
	public void save(Usrtrx usrtrx) {
		// TODO Auto-generated method stub
		usrtrxRepository.save(usrtrx);
	}

	@Override
	public void delete(Integer id) {
		// TODO Auto-generated method stub
		usrtrxRepository.deleteById(id);
	}

	@Override
	public Usrtrx getById(Integer id) {
		// TODO Auto-generated method stub
		return usrtrxRepository.findByIdUsrtrx(id);
	}

	@Override
	public List<Usrtrx> getAll() {
		// TODO Auto-generated method stub
		return usrtrxRepository.findAll();
	}

	@Override
	public String trxNumber() {
		// TODO Auto-generated method stub
		String th = new SimpleDateFormat("yy").format(new Date());
		String trxNum = "TRX"+ th + usrtrxRepository.getLastTrx();
		return trxNum;
	}

}

package com.ut.mti.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ut.mti.entity.Usrtrx;

@Service
public interface UsrtrxDAO {	
	
	void save(Usrtrx usrtrx);
	void delete(Integer id);
	Usrtrx getById(Integer id);
	List<Usrtrx> getAll();
	String trxNumber();

}

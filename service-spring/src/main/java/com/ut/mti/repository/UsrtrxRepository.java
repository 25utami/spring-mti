package com.ut.mti.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ut.mti.entity.Usrtrx;

@Repository
public interface UsrtrxRepository extends JpaRepository<Usrtrx, Integer>{
	
	Usrtrx findByIdUsrtrx(Integer idUsrtrx);
	
	@Query(value = "SELECT NEXT VALUE FOR sq_trx", nativeQuery = true)
	long getLastTrx();

}

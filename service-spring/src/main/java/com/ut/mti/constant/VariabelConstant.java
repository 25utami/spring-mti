package com.ut.mti.constant;

public class VariabelConstant {

	public static final String MTI					= "mti/";
	public static final String ADD					= MTI +"add";
	public static final String EDIT					= MTI +"edit";
	public static final String DELETE				= MTI +"delete";
	public static final String VIEW					= MTI +"view";
	public static final String GET_SINGLE			= MTI +"getsingle";
	public static final String DOWNLOAD				= MTI +"download";
}
